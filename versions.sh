#!/bin/sh
# 
# The only real pupose of this test script is to see the 
# latest version numbers and potentially validate that 
# the anticipated versions are installed

php -v | head -n1
git --version
git flow version
echo "node ("$(node -v)")"
echo "npm ("$(npm -v)")"
composer -V
terminus -V
echo "standard ("$(standard --version)")"
echo "eclint ("$(eclint --version)")"
echo "stylelint ("$(stylelint --version)")"
wp --version --allow-root
echo "jq ("$(jq --version)")"
