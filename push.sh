#!/bin/sh

GROUP="barrelny"
VERSION=$(sh version.sh)
REPO="multipurpose-ci"
USER="$1"
PASS="$2"

docker pull $GROUP/$REPO:latest || true
docker build -t $GROUP/$REPO .
docker tag $GROUP/$REPO $GROUP/$REPO:$VERSION

docker image ls | grep $REPO
docker login -u$USER -p$PASS
docker push $GROUP/$REPO:$VERSION
