#!/bin/sh

GROUP="barrelny"
VERSION=$(sh version.sh)
REPO="multipurpose-ci"

docker run -i -t $GROUP/$REPO:$VERSION /bin/bash
